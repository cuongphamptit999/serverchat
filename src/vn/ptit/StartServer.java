/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit;

import com.sun.corba.se.spi.activation.Server;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.ptit.controller.ClientHandler;

/**
 *
 * @author Cuong Pham
 */
public class StartServer {
    public static List<ClientHandler> clientHandlers;
    private static ServerSocket serverSocket;

    public StartServer() throws IOException {
        clientHandlers = new ArrayList<>();
        int port = 6969;
        serverSocket = new ServerSocket(port);
        System.out.println("Create server port "+port);
        while(true){
            Socket socket = serverSocket.accept();
            System.out.println("New client request received : " + socket);
            ClientHandler clientHandler = new ClientHandler(socket);
            clientHandlers.add(clientHandler);
            clientHandler.start();
        }
        
    }
    
    
    public static void main(String[] args) {       
        try {
            new StartServer();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
}
