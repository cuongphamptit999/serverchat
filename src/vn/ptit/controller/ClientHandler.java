/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.ptit.StartServer;
import vn.ptit.model.User;
import vn.ptit.util.StreamData;

/**
 *
 * @author Cuong Pham
 */
public class ClientHandler extends Thread {

    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private User user;

    public ClientHandler(Socket socket) throws IOException {
        this.socket = socket;
        this.dataInputStream = new DataInputStream(socket.getInputStream());
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        String received;
        while (true) {
            try {
                received = dataInputStream.readUTF();
                StreamData.Type type = StreamData.getTypeFromData(received);
                switch (type) {
                    case CONNECT_SERVER:
                        onReceiveConnectServer(received);
                        break;

                    case LOGIN:
                        onReceiveLogin(received);
                        break;
                    case LIST_ONLINE:
                        onReceiveListRoom(received);
                        break;
                    case CHAT_ROOM:
                        onReceiveChatRoom(received);
                        break;
                    case LOGOUT:
                        onReceiveLogout(received);
                        break;
                }
            } catch (IOException ex) {
                System.out.println(ex);
            }

        }
    }

    public void sendData(String data) {
        try {
            dataOutputStream.writeUTF(data);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private void onReceiveLogin(String received) {
        String[] splitted = received.split(";");

        String username = splitted[1];
        List<ClientHandler> clientHandlers = StartServer.clientHandlers;

        boolean isFlag = true;
        clientHandlers = StartServer.clientHandlers;
        for (ClientHandler clientHandler : clientHandlers) {
            if (clientHandler.user != null && clientHandler.user.getUsername().equalsIgnoreCase(username)) {
                isFlag = false;
                break;
            }
        }

        if (isFlag) {
            user = new User(username);
            sendData(StreamData.Type.LOGIN.name() + ";" + "success;" + username);
        } else {
            sendData(StreamData.Type.LOGIN.name() + ";" + "failure");
        }

    }

    private void onReceiveConnectServer(String received) {
        sendData(received);
    }

    private void onReceiveListRoom(String received) {
        List<ClientHandler> clientHandlers = StartServer.clientHandlers;
        String res = StreamData.Type.LIST_ONLINE.name() + ";success;";
        for (ClientHandler clientHandler : clientHandlers) {
            if (clientHandler.user != null) {
                res += clientHandler.user.getUsername() + ";";
            }
        }
        System.out.println(res);
        sendData(res);
    }

    private void onReceiveChatRoom(String received) {
        String[] splitted = received.split(";");
        String[] datas = splitted[1].split("\\#");
        if (datas.length == 2) {
            String chatMsg = datas[0];
            String recipient = datas[1];
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String data = StreamData.Type.CHAT_ROOM.name() + ";" + sdf.format(date) + ";"
                    + user.getUsername() + ";"
                    + chatMsg;
            System.out.println(data);
            for (ClientHandler clientHandler : StartServer.clientHandlers) {
                if (clientHandler.user.getUsername().equalsIgnoreCase(recipient)) {
                    clientHandler.sendData(data);
                    break;
                }
            }

        } else if (datas.length == 1) {
            String chatMsg = datas[0];
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String data = StreamData.Type.CHAT_ROOM.name() + ";" + sdf.format(date) + ";"
                    + user.getUsername() + ";"
                    + chatMsg;
            System.out.println(data);
            for (ClientHandler clientHandler : StartServer.clientHandlers) {
                clientHandler.sendData(data);
            }
        }
    }

    private void onReceiveLogout(String received) {
        user = null;
        sendData(StreamData.Type.LOGOUT.name() + ";success");
    }
}
